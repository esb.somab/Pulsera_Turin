"""
Código para graficar archivos leidos por la pulsera Turin
se lee un archivo csv
lineas nuevas separan cada serie de datos a graficar
el contenido de la variable delimitadorEncendido separa cada grupo de series de datos
en el archivo csv
la primera linea antes de cada serie de datos en el csv determina el nombre de la grafica
por lo tanto debe indicar al menos: nombreDelEjercicio, parteDelCuerpo
idealmente también carga de batería y temperatura
IMPORTANTE: primera linea del archivo debe estar vacía (por defecto en pulsera):
,,,,,,
NuevoEncendidoDelArduino,,,,,,
,,,,,,
"""

import csv
import os
import matplotlib.pyplot as plt#matplotlib solo es soportado hasta Python 3.6, instalar python 3.6 luego en cmd pip install matplotlib, automaticamente instala dependencias
"""
tampoco se soporta la ultima version de numpy
pip uninstall numpy
pip install numpy==1.19.3
"""
import numpy as np

#variables globales
tituloEjeX = "Número de muestra, muestreado a 104 Hz"
delimitadorEncendido = "NuevoEncendidoDelArduino"
nombreArchivo = "Rutinas"
numeroMuestra = []
bloque  = 0
xac = []
yac = []
zac = []
xgi = []
ygi = []
zgi = []
row =""
nuevaSerie=True # inicialmente True con declararla aca ya es global
titulo="" #con declararla aca ya es global
ejes=['x','y','z']


nombreArchivo = input("Ingrese el nombre del archivo a leer: ")
if nombreArchivo =="":
    nombreArchivo = "Rutinas"

#try:
with open(nombreArchivo) as csv_file:
    nombreCarpeta = "Graficas " +nombreArchivo
    if not os.path.exists(nombreCarpeta):
        os.makedirs(nombreCarpeta)

    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
        if nuevaSerie == True:#leer titulo
            if row[0] == delimitadorEncendido or row[1] == "":
                if row[0] == delimitadorEncendido:
                    bloque += 1 #aumentar contador de bloques
                    print("\nGraficando bloque de ejercicios: ", bloque)
                nuevaSerie=True
            else:
                nuevaSerie=False
                for i in row: #recorrer lista                   
                    titulo += i              
                titulo = titulo.replace('.', ',')#poner fuera del for
                titulo = titulo.replace(':', '')
        else:   #leer datos convirtiendolos a float e int porque sino se leen como String
            if row[0] != "":
                numeroMuestra.append(int(row[0]))
            if row[1] != "":
                xac.append(float(row[1]))
            if row[2] != "":
                yac.append(float(row[2]))
            if row[3] != "":
                zac.append(float(row[3]))
            if row[4] != "":
                xgi.append(float(row[4]))
            if row[5] != "":
                ygi.append(float(row[5]))
            if row[6] != "":
                zgi.append(float(row[6]))
            if row[0] in (None, ""):#linea vacia: leer inicio de serie o acaba de terminar serie anterior
            
                print("Graficando: " +titulo)
                nuevaSerie=True #para iniciar nueva serie               
    #                    titulo = "Acelerómetro " + titulo
                #graficar datos del acelerómetro
                fig=plt.figure()
                plt.plot(numeroMuestra, xac, label='eje x', color='g')  # Plot some data on the (implicit) axes.
                plt.plot(numeroMuestra, yac, label='eje y', color='b')  # 
                plt.plot(numeroMuestra, zac, label='eje z', color='orange')
                plt.xlabel(tituloEjeX)
                plt.ylabel('Gravedad (g)')
                plt.grid(b=None, which='major', axis='y')
                plt.title("Acelerómetro Bloque " +str(bloque) + " " + titulo)
                plt.legend()                 
                plt.xticks(np.arange(min(numeroMuestra), max(numeroMuestra)+1, 100.0), rotation='vertical')#eje x entre extremos de variables numeroMuestra medidas separadas cada 100 unidades                   
                #fig = plt.gcf()#una vez que figura fue graficada
                fig.set_size_inches(18.1, 9.1)#dimension en pulgadas de grafica
                tituloImagen = nombreCarpeta + '/' +str("Acelerómetro Bloque " + str(bloque) + " " +  titulo)
    #                    fig.savefig(tituloImagen, dpi=200)
                plt.savefig(tituloImagen, dpi=150)
    #                    plt.show()#descomentar si quiero que muestre grafica
                plt.close(fig)
                #borrar variables para nuevo ciclo
                #graficar datos del giroscopio
                fig=plt.figure()
                plt.plot(numeroMuestra, xgi, label='eje x', color='limegreen')  # Plot some data on the (implicit) axes.
                plt.plot(numeroMuestra, ygi, label='eje y', color='cyan')  # 
                plt.plot(numeroMuestra, zgi, label='eje z', color='darkorange')
                plt.xlabel(tituloEjeX)
                plt.ylabel('Velocidad angular (grados/s)')
                plt.grid(b=None, which='major', axis='y')
                plt.title("Giroscopio Bloque " + str(bloque) + " " + titulo)
                plt.legend()                 
                plt.xticks(np.arange(min(numeroMuestra), max(numeroMuestra)+1, 100.0), rotation='vertical')#eje x entre extremos de variables numeroMuestra medidas separadas cada 100 unidades                   
                #fig = plt.gcf()#una vez que figura fue graficada
                fig.set_size_inches(18.1, 9.1)#dimension en pulgadas de grafica
                tituloImagen = nombreCarpeta + '/' +str("Giroscopio Bloque " + str(bloque) + " " + titulo)
    #                    fig.savefig(tituloImagen, dpi=200)
                plt.savefig(tituloImagen, dpi=150)
    #                    plt.show()#descomentar si quiero que muestre grafica
                plt.close(fig)

                xac.clear()
                yac.clear()
                zac.clear()
                xgi.clear()
                ygi.clear()
                zgi.clear()
                titulo = ""
                numeroMuestra.clear()
    #graficar ultima serie que es ignorada en el loop cuando termina el archivo
    print("Graficando: " +titulo)
    nuevaSerie=True #para iniciar nueva serie               
#                    titulo = "Acelerómetro " + titulo
    #graficar datos del acelerómetro
    fig=plt.figure()
    plt.plot(numeroMuestra, xac, label='eje x', color='g')  # Plot some data on the (implicit) axes.
    plt.plot(numeroMuestra, yac, label='eje y', color='b')  # 
    plt.plot(numeroMuestra, zac, label='eje z', color='orange')
    plt.xlabel(tituloEjeX)
    plt.ylabel('Gravedad (g)')
    plt.grid(b=None, which='major', axis='y')
    plt.title("Acelerómetro Bloque " +str(bloque) + " " + titulo)
    plt.legend()                 
    plt.xticks(np.arange(min(numeroMuestra), max(numeroMuestra)+1, 100.0), rotation='vertical')#eje x entre extremos de variables numeroMuestra medidas separadas cada 100 unidades                   
    #fig = plt.gcf()#una vez que figura fue graficada
    fig.set_size_inches(18.1, 9.1)#dimension en pulgadas de grafica
    tituloImagen = nombreCarpeta + '/' +str("Acelerómetro Bloque " + str(bloque) + " " +  titulo)
#                    fig.savefig(tituloImagen, dpi=200)
    plt.savefig(tituloImagen, dpi=150)
#                    plt.show()#descomentar si quiero que muestre grafica
    plt.close(fig)
    #borrar variables para nuevo ciclo
    #graficar datos del giroscopio
    fig=plt.figure()
    plt.plot(numeroMuestra, xgi, label='eje x', color='limegreen')  # Plot some data on the (implicit) axes.
    plt.plot(numeroMuestra, ygi, label='eje y', color='cyan')  # 
    plt.plot(numeroMuestra, zgi, label='eje z', color='darkorange')
    plt.xlabel(tituloEjeX)
    plt.ylabel('Velocidad angular (grados/s)')
    plt.grid(b=None, which='major', axis='y')
    plt.title("Giroscopio Bloque " + str(bloque) + " " + titulo)
    plt.legend()                 
    plt.xticks(np.arange(min(numeroMuestra), max(numeroMuestra)+1, 100.0), rotation='vertical')#eje x entre extremos de variables numeroMuestra medidas separadas cada 100 unidades                   
    #fig = plt.gcf()#una vez que figura fue graficada
    fig.set_size_inches(18.1, 9.1)#dimension en pulgadas de grafica
    tituloImagen = nombreCarpeta + '/' +str("Giroscopio Bloque " + str(bloque) + " " + titulo)
#                    fig.savefig(tituloImagen, dpi=200)
    plt.savefig(tituloImagen, dpi=150)
#                    plt.show()#descomentar si quiero que muestre grafica
    plt.close(fig)             
print("\nGraficación terminada")
print("Archivos guardados en la carpeta: " +nombreCarpeta)