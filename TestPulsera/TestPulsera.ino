//Codigo para probar pulsera Turin V1
//solo se puede leer la carga de la batería en el modo ON
//Direccion con mapeo de pines
//C:\Users\***\AppData\Local\Arduino15\packages\arduino\hardware\mbed\1.1.6\variants\ARDUINO_NANO33BLE

//bibliotecas
#include "Arduino.h"

//IMU en Arduino
#include <Arduino_LSM9DS1.h>

//uSD
#include <SPI.h>
#include <SD.h>

//Pulsioximetro
#include <Wire.h>
#include "MAX30105.h"

//variables
unsigned long contadorRebotes, contadorRebotesAnterior;
byte estado = 0;
float x, y, z, carga;
boolean graficadorSerial=false;      //verdadero para reducir impresiones y ver graficas bien

bool Robin = false;   //si no soy Robin

//constantes de conexiones
const byte motor = 2; //pin de la compuerta del transistor
const int chipSelect = 4; //codigo de uSD
byte boton = 3; //pin del boton real 3
byte bateria = A7; //pin del divisor de tension real A7


//objetos
MAX30105 particleSensor;
File dataFile;

//cofig IMU


//The setup function is called once at startup of the sketch
void setup()
{
  if(Robin){//Robin tiene pulsera diferente
    boton = 5;
    bateria = A6;// 20 pin correspondiente a A6
  }
  
  Serial.begin(115200);
   while (!Serial){;}

   pinMode(motor, OUTPUT);
   digitalWrite(motor, LOW);
   pinMode(boton, INPUT_PULLUP);

   Serial.println("Iniciando banco de pruebas");

   attachInterrupt(digitalPinToInterrupt(boton), ISRBoton, FALLING);

    //pulsioximetro
      if (!particleSensor.begin(Wire, I2C_SPEED_FAST)) //Use default I2C port, 400kHz speed
    {
      Serial.println("MAX30105 was not found. Please check wiring/power. ");
      while (1);
    }
   //Setup to sense a nice looking saw tooth on the plotter
    byte ledBrightness = 0x1F; //Options: 0=Off to 255=50mA
    byte sampleAverage = 8; //Options: 1, 2, 4, 8, 16, 32
    byte ledMode = 3; //Options: 1 = Red only, 2 = Red + IR, 3 = Red + IR + Green
    int sampleRate = 100; //Options: 50, 100, 200, 400, 800, 1000, 1600, 3200
    int pulseWidth = 411; //Options: 69, 118, 215, 411
    int adcRange = 4096; //Options: 2048, 4096, 8192, 16384
  
    particleSensor.setup(ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange); //Configure sensor with these settings
  
    //Arduino plotter auto-scales annoyingly. To get around this, pre-populate
    //the plotter with 500 of an average reading from the sensor
  
    //Take an average of IR readings at power up
    const byte avgAmount = 64;
    long baseValue = 0;
    for (byte x = 0 ; x < avgAmount ; x++)
    {
      baseValue += particleSensor.getIR(); //Read the IR value
    }
    baseValue /= avgAmount;
  
    //Pre-populate the plotter so that the Y scale is close to IR values
    for (int x = 0 ; x < 500 ; x++)
      Serial.println(baseValue);

    //Lector de uSD
      Serial.print("Initializing SD card...");
    // see if the card is present and can be initialized:
    if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    while (1);
    }
    Serial.println("card initialized.");
    //open the file. note that only one file can be open at a time,
    // so you have to close this one before opening another.
    dataFile = SD.open("datalog.txt");                    //Creacion del objeto
    // if the file is available, write to it:
    if (dataFile) {
      ;
    }
    // if the file isn't open, pop up an error:
    else {
      Serial.println("error opening datalog.txt");
    }

    //IMU
    if (!IMU.begin()) {//por defecto lee menos g hay que configurarlo al máximo
    Serial.println("Failed to initialize IMU!");
    while (1);
    }
    
    //Acelerometro
    //Biblioteca nueva, si no se pone simplemente compila con valores por defecto de biblioteca    
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                  REEMPLAZAR LAS DOS LINEAS DE ABAJO CON LOS RESULTANTES DE LA CALIBRACION                                    
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    IMU.setAccelFS(1);
    IMU.setAccelODR(3);
    //IMU.setAccelOffset(-0.024080, -0.000375, -0.032896);
    //IMU.setAccelSlope (0.986607, 0.984625, 0.993664);
    IMU.setAccelOffset(0, 0, 0);  //   uncalibrated
    IMU.setAccelSlope (1, 1, 1);  //   uncalibrated
    IMU.accelUnit=  GRAVITY;    // or  METERPERSECOND2
    
    if (!graficadorSerial){
      Serial.print("Accelerometer sample rate = ");
      Serial.print(IMU.accelerationSampleRate());
      Serial.println(" Hz");
      Serial.println();
      Serial.println("Acceleration in G's");
      Serial.println(IMU.getAccelFS());
      Serial.println("X\tY\tZ");
    }

    //Giroscopio
    IMU.gyroUnit= DEGREEPERSECOND;  //   DEGREEPERSECOND  RADIANSPERSECOND  REVSPERMINUTE  REVSPERSECOND  
    if (!graficadorSerial)
    {  Serial.println("Gyroscope in degrees/second \n");
       Serial.print("Gyroscope Full Scale = ±");
       Serial.print(IMU.getGyroFS());
       Serial.println ("°/s");
       Serial.print("Gyroscope sample rate = ");
       Serial.print(IMU.getGyroODR());        //alias  IMU.gyroscopeSampleRate());
       Serial.println(" Hz");
       delay(4000);
    }
    
}


void loop()
{
  switch(estado){

  case 0:
    Serial.print("Prueba pulsioximetro\t");//abrir graficador serial, comentar esta linea para tener zoom en señal
    Serial.println(particleSensor.getIR());
    break;

  case 1:
    Serial.println("Prueba tarjeta microSD");  
    if (dataFile) {
    while (dataFile.available()) {
      Serial.write(dataFile.read());
    }
    dataFile.close();
    }
    // if the file isn't open, pop up an error:
    else {
    Serial.println("error opening datalog.txt");
    Serial.println("Reabriendo archivo");
    dataFile = SD.open("datalog.txt");                    //Creacion del objeto
    }
    break;

  case 2:
    Serial.print("Prueba bateria\t");
    carga = analogRead(bateria);
    Serial.print("Valor leido: ");
    Serial.print(carga);
    carga = carga*(3.3/1023)*2;
    Serial.print("\tTension: ");
    Serial.println(carga);
    break;

  case 3:
    if (IMU.accelerationAvailable()) {//abrir graficador serial
      Serial.print("Prueba accelerómetro\tx: ");
      IMU.readAcceleration(x, y, z);
      Serial.print(x);
      Serial.print("\ty: ");//comentar lineas que no son variables para ver bien señales en graficador
      Serial.print(y);
      Serial.print("\tz: ");
      Serial.println(z);
    }
    break;

  case 4:
    if (IMU.gyroAvailable())    // alias IMU.gyroscopeAvailable
    {  
      Serial.print("Prueba girsocopio\tx: ");
      IMU.readGyro(x, y, z);   // alias IMU.readGyroscope
      Serial.print(x);
      Serial.print("\ty: ");
      Serial.print(y);
      Serial.print("\tz: ");
      Serial.println(z);
    }
    break;    

  case 5:
    Serial.println("Prueba motor");
      for(int x = 0; x<256;x++){
        Serial.println(x);
        delay(5);
      analogWrite(motor,x);
      }
      for(int x = 255; x>0;x--){
        Serial.println(x);
        delay(5);
      analogWrite(motor,x);
      }    
    break;
  }

}

//funciones propias

void ISRBoton(){
  contadorRebotes = millis();
  if (contadorRebotes - contadorRebotesAnterior > 200)
  {
    ++estado;  
    if(estado > 5){
      estado = 0;
    }
  }
  contadorRebotesAnterior = contadorRebotes;
}