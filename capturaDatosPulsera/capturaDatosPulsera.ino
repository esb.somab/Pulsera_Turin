/*
 * Recopilador de datos de aceletrometro y giroscopio para analisis posterior
  Basado en ejemplo "SD card datalogger" de la biblioteca SD por Tom Igoe

  Codigo de colores:
  rojo continuo: en espera
  Azula parpadeando: a punto de comenzar recopilacion de datos
  verde continuo: recopilando datos
  Parpadean rapido rojo y azul: no hay tarjeta SD : reinicar y colocar SD
  Parpadean rapido rojo y verde: no se detecta IMU (fatal): rezar a Dios
*/

//Bibliotecas

//IMU en Arduino
#include <Arduino_LSM9DS1.h>

//uSD
#include <SPI.h>
#include <SD.h>


//variables
unsigned long contadorRebotes, contadorRebotesAnterior;
int serie = 0;
byte estado = 0;
float x, y, z, carga;
String nombreArchivo = "rutinas";
long numeroLinea;

bool Robin = false;//si no soy Robin

//Constantes de conexiones
const int ledRojo = 22;
const int ledVerde = 23;
const int ledAzul = 24;
const byte motor = 2; //pin de la compuerta del transistor
const int chipSelect = 4;
byte boton = 3;
byte bateria = A7;

//objetos
File dataFile;

void setup() {
  
  if(Robin){//Robin tiene pulsera diferente
    boton = 5;
    bateria = A6;//pin correspondiente a A6
  } 
  
  pinMode(motor, OUTPUT);
  digitalWrite(motor, LOW);
  pinMode(boton, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(boton), ISRBoton, FALLING);
   
  pinMode(ledRojo,OUTPUT);
  pinMode(ledVerde,OUTPUT);
  pinMode(ledAzul,OUTPUT);

  digitalWrite(ledRojo, HIGH);//rojo
  digitalWrite(ledVerde, HIGH);//verde
  digitalWrite(ledAzul, HIGH);//azul

  //IMU
  if (!IMU.begin()) {//por defecto lee menos g hay que configurarlo al máximo
  Serial.println("Failed to initialize IMU!");
  while (1){
     digitalWrite(ledRojo,LOW);
     digitalWrite(ledVerde,HIGH);
     delay(150);
     digitalWrite(ledAzul,LOW);
     digitalWrite(ledVerde,HIGH);
     delay(150);
    }
  }
    
  //Acelerometro
  //Biblioteca nueva, si no se pone simplemente compila con valores por defecto de biblioteca    
  
  IMU.setAccelFS(1);// 16 g
  IMU.setAccelODR(3);//usar 119 Hz
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                  REEMPLAZAR LAS DOS LINEAS DE ABAJO CON LOS RESULTANTES DE LA CALIBRACION                                    
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Robin
  IMU.setAccelOffset(-0.024080, -0.000375, -0.032896);
  IMU.setAccelSlope (0.986607, 0.984625, 0.993664);

//Edwin  


//Pablo


////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
  IMU.accelUnit=  GRAVITY;    // or  METERPERSECOND2
  //Giroscopio
  IMU.gyroUnit= DEGREEPERSECOND;  //   DEGREEPERSECOND  RADIANSPERSECOND  REVSPERMINUTE  REVSPERSECOND  
  
  Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // loop infinito si no hay SD
    while (1){
      digitalWrite(ledRojo,LOW);
      digitalWrite(ledAzul,HIGH);
      delay(150);
      digitalWrite(ledAzul,LOW);
      digitalWrite(ledRojo,HIGH);
      delay(150);
      }
  }
  Serial.println("card initialized.");
  //Crear la instancia del archivo para la SD
  dataFile = SD.open(nombreArchivo,FILE_WRITE);                    //inicializacion
  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println("\nNuevoEncendidoDelArduino");//imprimir siempre una linea nueva tras el corte de datos
    dataFile.close();
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("Error abriendo el archivo");
  }

  
}

void loop() {

  
  switch(estado){
    
    case 0:{
    digitalWrite(ledRojo, LOW);
    digitalWrite(ledVerde,HIGH);
    digitalWrite(ledAzul,HIGH);
    }
    break;
    
    case 1:{
      //while(estado==1){// para que si se apreta boton mientra inicia se salga
        
      
      digitalWrite(ledRojo, HIGH);
      numeroLinea = 0;
      float datos[6]; //arreglo de 6 flotantes
      String stringDatos = "";
      for(int c=0; c<6; c++){
        digitalWrite(ledAzul, !digitalRead(ledAzul));
        delay(300);  
      }
      
      if(estado!=1)break;//su hubo rebotes durante el parpadeado del led azul que cambiaron el estado cancelar
      //tambien se cancela si el usuario presiono el boton mientras parpadeada el led azul
      
      digitalWrite(ledVerde, LOW);
      dataFile = SD.open(nombreArchivo,FILE_WRITE);//si archivo no existe lo crea
      stringDatos = "\nSerie ";//imprimir siempre una linea nueva tras el corte de datos
      serie++;  //aumentar contador a ser impreso al iniciar serie
      stringDatos += String(serie);
      //dataFile.print(stringDatos);
      carga = 0;
      for(int c =0; c<10;c++){//sacar un promedio de 10 lecturas
        carga += analogRead(bateria);
      }
      carga = carga/10;
      carga = carga*(3.3/1023)*2;
      stringDatos += ", Carga de bateria ";//se borró : ya que causa problemas al aguardar imagen en python
      stringDatos += String(carga);
      dataFile.println(stringDatos);
      digitalWrite(motor,HIGH);
      delay(500);
      digitalWrite(motor,LOW);
  
      
      while(estado==1){
        numeroLinea++;
        stringDatos = String(numeroLinea); //borrar string y poner numero de muestra
        stringDatos += ',';
        while(!IMU.accelerationAvailable())
        {
          //dejar que espere a que hallan datos porque codigo corre mas rapido que tasa de muestreo usada
        }
          IMU.readAcceleration(x, y, z);
          datos[0] = x;
          datos[1] = y;
          datos[2] = z;
        while(!IMU.gyroAvailable())    // alias IMU.gyroscopeAvailable
        {
          
        }
          IMU.readGyro(x, y, z);   // alias IMU.readGyroscope
          datos[3] = x;
          datos[4] = y;
          datos[5] = z;
        
        for (int i = 0; i < 6; i++) {
          stringDatos += String(datos[i]);
          if (i < 5) {
            stringDatos += ",";
          }
        }
          if (dataFile) {
            dataFile.println(stringDatos);
          }
      }
      dataFile.close();
      //}
    }
    break;
    
    case 2:{
    detachInterrupt(digitalPinToInterrupt(boton));
    estado=0;
    digitalWrite(ledRojo, HIGH);
    digitalWrite(motor,HIGH);
    delay(200);
    digitalWrite(motor,LOW);
    delay(150);
    digitalWrite(motor,HIGH);
    delay(200);
    digitalWrite(motor,LOW);
    attachInterrupt(digitalPinToInterrupt(boton), ISRBoton, FALLING);
    }
    break;
    
    default:{
    estado = 0;
    }
  }
/*  if(estado==2){//lectura terminada

    }*/

}

//funciones propias

void ISRBoton(){
  contadorRebotes = millis();
  if (contadorRebotes - contadorRebotesAnterior > 200)
  {
    estado++;  
  }
  contadorRebotesAnterior = contadorRebotes;
}
